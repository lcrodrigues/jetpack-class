package com.example.dogs.model

class SmsInfo(
    var to: String,
    var text: String,
    var imageUrl: String?
)