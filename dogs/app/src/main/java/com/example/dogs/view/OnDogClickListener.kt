package com.example.dogs.view

import android.view.View

interface OnDogClickListener {
    fun onDogClicked(v: View)
}